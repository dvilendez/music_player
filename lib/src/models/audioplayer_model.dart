import 'package:flutter/material.dart';

class AudioPlayerModel with ChangeNotifier {

  bool _isPlaying        = false;
  Duration _songDuration = new Duration(milliseconds: 0);
  Duration _current      = new Duration(milliseconds: 0);
  AnimationController _controller;


  String get songTotalDuration => this.printDuration(this._songDuration);
  String get currentSecond => this.printDuration(this._current);

  double get percentage => (this._songDuration.inSeconds != 0)
                              ? this._current.inSeconds / this._songDuration.inSeconds
                              : 0;

  bool get isPlaying => this._isPlaying;
  Duration get songDuration => this._songDuration;
  Duration get current => this._current;
  AnimationController get controller => this._controller;


  set isPlaying (bool isPlaying) {
    this._isPlaying = isPlaying;
    notifyListeners();
  }

  set songDuration (Duration songDuration) {
    this._songDuration = songDuration;
    notifyListeners();
  }

  set current (Duration current) {
    this._current = current;
    notifyListeners();
  }

  set controller (AnimationController controller) {
    this._controller = controller;
  }

  String printDuration(Duration duration) {

    String twoDigits(int n) {
      if (n>= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));

    return "$twoDigitMinutes:$twoDigitSeconds";

  }

}